#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List
from sys import stdin, stdout

number_list = [0]*1000001


# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range,
    [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # i and j should be between 0 and 1000000
    assert i > 0
    assert i <= 1000000
    assert j > 0
    assert j <= 1000000
    max_cycle_length = 0
    """
    this loop calls cycle_length() for
    every value on the interval (inclusive)
    and returns the greatest length
    """
    if(j < i):
        y = i
        i = j
        j = y
        # this is the switcheroo so i < j
    for y in range(i, j + 1):
        curr_cycle_length = number_list[y]
        assert curr_cycle_length > 0
        if curr_cycle_length > max_cycle_length:
            max_cycle_length = curr_cycle_length

    return max_cycle_length

# ------------
# cycle_length
# this actually calculates the cycle length of the int passed (recursive)
# i is the number to be used to calculate cycle length
# count is the variable that holds the current cycle length, on each call it c
# hecks if i is 1 otherwise it just performs the rules and calls itself again,
# adding 1 to the count
# ------------


def cycle_length(i: int) -> int:
    numbers = []
    cycle = 0
    while i != 1:
        if(i <= 1000000):
            # check if it's already in the list
            poss_cycle = number_list[i]
            if(poss_cycle > 0):
                # already in the list
                total = cycle + poss_cycle
                add_dictionary(numbers, total)
                return cycle + poss_cycle   
        numbers.append(i)
        cycle = cycle + 1
        if(i % 2) == 0:
            # even number
            i = i // 2
        else:
            # odd number
            i = (3 * i) + 1
    cycle = cycle + 1
    add_dictionary(numbers, cycle)
    return cycle


def add_dictionary(numbers: List[int], cycle_length: int) -> None:
    for y in numbers:
        if y <= 1000000:
            number_list[y] = cycle_length
        cycle_length = cycle_length - 1

# -------------
# collatz_print
# -------------

def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)


def main() -> None:
    for i in range(1, 1000001):
        number_list[i] = cycle_length(i)
    collatz_solve(stdin, stdout)

if __name__ == "__main__":
    main()